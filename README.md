# About #

This repository contains the utilities, which I wrote to address few problems I face.

# Utilities Inside #


  + [CopyAcrossClientbox (JAVA)](https://bitbucket.org/sundarkp/sundarkpcode/tree/master/CopyAcrossClientBox%5BJava%5D) - [Read me](https://bitbucket.org/sundarkp/sundarkpcode/blob/master/CopyAcrossClientBox%5BJava%5D/Readme.md), [Demo](http://www.youtube.com/watch?feature=player_embedded&v=pwNZNmLc5Ls)
  + [Rediff Viewer (Chrome Extension)](https://bitbucket.org/sundarkp/sundarkpcode/tree/master/RediffViewer%5BChrome%20Extn%5D) - [Read me](https://bitbucket.org/sundarkp/sundarkpcode/blob/master/RediffViewer%5BChrome%20Extn%5D/Readme.md), [Demo](http://www.youtube.com/watch?feature=player_embedded&v=SK9u9ozI-CE), [Download](https://chrome.google.com/webstore/detail/rediff-viewer/iljopgjppjojkganmkfokfcdbkedoehn)
  + [SPY Translator      (C++, C#, COM)](https://bitbucket.org/sundarkp/sundarkpcode/tree/master/SPY_Translator%5BCPP_COM%5D) - [Read me](https://bitbucket.org/sundarkp/sundarkpcode/blob/master/SPY_Translator%5BCPP_COM%5D/Readme.md), [Demo](http://www.youtube.com/watch?feature=player_embedded&v=vHyN0FkzYnk)
  + [Gmail Browser Open Pages  (Chrome Extension)](https://bitbucket.org/sundarkp/sundarkpcode/tree/master/SendOpenURLs%20%5BChrome%20Extn%5D) - [Read me](https://bitbucket.org/sundarkp/sundarkpcode/blob/master/SendOpenURLs%20%5BChrome%20Extn%5D/Readme.md), [Demo](http://www.youtube.com/watch?feature=player_embedded&v=i3Cxm4Xi6TM), [Download](https://chrome.google.com/webstore/detail/send-open-urls/ohapipgmanomnljkmlkainclgblifagk)
  + [LockTabs2Switch (Chrome Extension)](https://bitbucket.org/sundarkp/sundarkpcode/tree/master/LockTabs2Switch%5BChrome%20Extn%5D) - [Read me](https://bitbucket.org/sundarkp/sundarkpcode/blob/master/LockTabs2Switch%5BChrome%20Extn%5D/Readme.md), [Demo](http://www.youtube.com/watch?v=Xpa2uwZxe9A), [Download](https://chrome.google.com/webstore/detail/locktabs2switch/pllchchhoedgkdmhebopllldeendeoga)
  + [YoutubePlaylister      (Python DJango Project)](https://bitbucket.org/sundarkp/sundarkpcode/blob/master/YoutubePlayLister%5BPython%20Django%5D) -  
[Read me](https://bitbucket.org/sundarkp/sundarkpcode/blob/master/YoutubePlayLister%5BPython%20Django%5D/Readme.md) 

# Usage #

Please refer to the individual projects for details.

# License #
New APACHE License - Copyright(c) 2014, Sundara Kumar Padmanabhan. 
See [License](http://www.apache.org/licenses/LICENSE-2.0.html) for details.

# Author #

[Sundara Kumar Padmanabhan](http://www.linkedin.com/in/sundarkp)




